/*
 * File:   main.c
 * Author: Andres jacob
 *
 * Created on 15 de octubre de 2021, 12:43 PM
 */

#include <stdio.h>
#include <stdlib.h>
#define FCY 5000000
#include <libpic30.h>
#include "config.h"

int numero=0;
int digitoU=0;
int digitoD=0;
int band=0;

void main () {
    
    // CONFIGURACION PUERTOS
    
    //entradas fila
    TRISBbits.TRISB5=1;
    TRISBbits.TRISB7=1; //entradas fia1
    TRISBbits.TRISB8=1;
    TRISBbits.TRISB9=1;
    
    //saildas columnas
    TRISAbits.TRISA0=0;
    TRISAbits.TRISA1=0;
    TRISAbits.TRISA2=0;
    
     //SALIDA MULTIPLEXADA
    TRISBbits.TRISB1=0;
    TRISBbits.TRISB4=0;
    

    //salidas 7 seg
    TRISBbits.TRISB14=0; //G
    TRISBbits.TRISB15=0; //F
    TRISBbits.TRISB11=0; //E
    TRISBbits.TRISB10=0; //D
    TRISBbits.TRISB13=0; //C
    TRISBbits.TRISB2=0;  //B
    TRISBbits.TRISB3=0;  //A      
    
    PORTB=0X000;
    PORTA=0X000;
    
    while(1){
        
        // poner columna 1 en alto para leer filas
        LATAbits.LATA0=1;
        LATAbits.LATA1=0;
        LATAbits.LATA2=0;
        __delay_ms(5);
        leer1();
        // poner columna 2 en alto para leer filas
        LATAbits.LATA0=0;
        LATAbits.LATA1=1;
        LATAbits.LATA2=0;
        leer2();
        __delay_ms(5);
        // poner columna 3 en alto para leer filas
        LATAbits.LATA0=0;
        LATAbits.LATA1=0;
        LATAbits.LATA2=1;
        leer3(); 
        __delay_ms(5);
        
        visu();
        imU();
        imD();
    }    
}

void leer1(){
    if(PORTBbits.RB5==1){
        while(PORTBbits.RB5==1){}
        numero=1;
        band=band+1;
    }
    if(PORTBbits.RB7==1){
        while(PORTBbits.RB7==1){}
        numero=4;
        band=band+1;
    }
    if(PORTBbits.RB8==1){
        while(PORTBbits.RB8==1){}
        numero=7;
        band=band+1;
    }
    if(PORTBbits.RB9==1){
        while(PORTBbits.RB9==1){}
        numero=0;
        band=band+1;
    }

}
void leer2(){
    if(PORTBbits.RB5==1){
        while(PORTBbits.RB5==1){}
        numero=2;
        band=band+1;
    }
    if(PORTBbits.RB7==1){
        while(PORTBbits.RB7==1){}
        numero=5;
        band=band+1;
    }
    if(PORTBbits.RB8==1){
        while(PORTBbits.RB8==1){}
        numero=8;
        band=band+1;
    }
    if(PORTBbits.RB9==1){
        while(PORTBbits.RB9==1){}
        numero=0;
        band=band+1;
    }
}
void leer3(){
    if(PORTBbits.RB5==1){
        while(PORTBbits.RB5==1){}
        numero=3;
        band=band+1;
    }
    if(PORTBbits.RB7==1){
        while(PORTBbits.RB7==1){}
        numero=6;
        band=band+1;
    }
    if(PORTBbits.RB8==1){
        while(PORTBbits.RB8==1){}
        numero=9;
        band=band+1;
    }
    if(PORTBbits.RB9==1){
        while(PORTBbits.RB9==1){}
        numero=0;
        band=band+1;
    }
}

void visu(){
    if (band==1){
        digitoU=numero;

    }
    if (band==2){
        digitoD=numero;
    }
    
    if (band>2){
        band=0;
    }
}

void imU(){
    
    LATBbits.LATB1=1;
    LATBbits.LATB4=0;
    imprimir(digitoU);  
    __delay_ms(5);
}

void imD(){
    
    LATBbits.LATB1=0;
    LATBbits.LATB4=1;
    imprimir(digitoD); 
    __delay_ms(5);
}

void imprimir (int cont){
    
    if (cont==0){
    cero();
    }
    if (cont==1){
    uno();
    }
    if (cont==2){
    dos();
    }
    if (cont==3){
    tres();
    }
    if (cont==4){
    cuatro();
    }
    if (cont==5){
    cinco();
    }
    if (cont==6){
    seis();
    }
    if (cont==7){
    siete();
    }
    if (cont==8){
    ocho();
    }
    if (cont==9){
    nueve();
    }
}

void uno () {
    LATBbits.LATB14=0; //G
    LATBbits.LATB15=0; //F
    LATBbits.LATB11=0; //E
    LATBbits.LATB10=0; //D
    LATBbits.LATB13=1; //C
    LATBbits.LATB2=1;  //B
    LATBbits.LATB3=0;  //A
}

void dos () {
    LATBbits.LATB14=1; //G
    LATBbits.LATB15=0; //F
    LATBbits.LATB11=1; //E
    LATBbits.LATB10=1; //D
    LATBbits.LATB13=0; //C
    LATBbits.LATB2=1;  //B
    LATBbits.LATB3=1;  //A
}

void tres () {
    LATBbits.LATB14=1; //G
    LATBbits.LATB15=0; //F
    LATBbits.LATB11=0; //E
    LATBbits.LATB10=1; //D
    LATBbits.LATB13=1; //C
    LATBbits.LATB2=1;  //B
    LATBbits.LATB3=1;  //A 
}

void cuatro () {
    LATBbits.LATB14=1; //G
    LATBbits.LATB15=1; //F
    LATBbits.LATB11=0; //E
    LATBbits.LATB10=0; //D
    LATBbits.LATB13=1; //C
    LATBbits.LATB2=1;  //B
    LATBbits.LATB3=0;  //A  
}

void cinco () {
    LATBbits.LATB14=1; //G
    LATBbits.LATB15=1; //F
    LATBbits.LATB11=0; //E
    LATBbits.LATB10=1; //D
    LATBbits.LATB13=1; //C
    LATBbits.LATB2=0;  //B
    LATBbits.LATB3=1;  //A 
    __delay_ms(100);    
}

void seis () {
    LATBbits.LATB14=1; //G
    LATBbits.LATB15=1; //F
    LATBbits.LATB11=1; //E
    LATBbits.LATB10=1; //D
    LATBbits.LATB13=1; //C
    LATBbits.LATB2=0;  //B
    LATBbits.LATB3=1;  //A 
}

void siete () {
    LATBbits.LATB14=0; //G
    LATBbits.LATB15=0; //F
    LATBbits.LATB11=0; //E
    LATBbits.LATB10=0; //D
    LATBbits.LATB13=1; //C
    LATBbits.LATB2=1;  //B
    LATBbits.LATB3=1;  //A 
}

void ocho () {
    LATBbits.LATB14=1; //G
    LATBbits.LATB15=1; //F
    LATBbits.LATB11=1; //E
    LATBbits.LATB10=1; //D
    LATBbits.LATB13=1; //C
    LATBbits.LATB2=1;  //B
    LATBbits.LATB3=1;  //A    
}

void nueve () {
    LATBbits.LATB14=1; //G
    LATBbits.LATB15=1; //F
    LATBbits.LATB11=0; //E
    LATBbits.LATB10=0; //D
    LATBbits.LATB13=1; //C
    LATBbits.LATB2=1;  //B
    LATBbits.LATB3=1;  //A    
}

void cero () {
    LATBbits.LATB14=0; //G
    LATBbits.LATB15=1; //F
    LATBbits.LATB11=1; //E
    LATBbits.LATB10=1; //D
    LATBbits.LATB13=1; //C
    LATBbits.LATB2=1;  //B
    LATBbits.LATB3=1;  //A    
}


